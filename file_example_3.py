with open("data/text_1.txt") as f:
    line = f.readline()
    while line != "":
        print(line, end="")
        line = f.readline()

print("-" * 120)
with open("data/text_1.txt") as f:
    for line in f.readlines():
        print(line, end="")

print("-" * 120)

with open("data/text_1.txt") as f:
    for line in f:
        print(line, end="")