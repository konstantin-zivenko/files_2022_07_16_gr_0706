with open("text_1.txt", "r") as file:
    print(file.read())
    print("-" * 120)

with open("text_1.txt", "r") as file:
    print(file.readline())
    print("-" * 120)

with open("text_1.txt", "r") as file:
    print(file.readlines())
    print("-" * 120)
